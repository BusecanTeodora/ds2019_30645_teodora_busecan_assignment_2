package com.example.springdemo.repositories;

import com.example.springdemo.entities.Intake;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntakeRepository extends JpaRepository<Intake, Integer> {
}
