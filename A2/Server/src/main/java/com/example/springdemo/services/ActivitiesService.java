package com.example.springdemo.services;

import com.example.springdemo.dto.ActivitiesDTO;
import com.example.springdemo.entities.Activities;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class ActivitiesService {

    private final ActivitiesRepository activitiesRepository;
    private final PatientRepository patientRepository;


    @Autowired
    public ActivitiesService(ActivitiesRepository activitiesRepository, PatientRepository patientRepository) {

        this.activitiesRepository = activitiesRepository;
        this.patientRepository = patientRepository;
    }

    public Activities myFunc(ActivitiesDTO activitiesDTO) {
        Patient patient = patientRepository.getOne(activitiesDTO.getPatient());
        Activities activities1 = new Activities(activitiesDTO.getIdactivities(), patient, activitiesDTO.getStart_time(), activitiesDTO.getEnd_time(), activitiesDTO.getActivity());
        activitiesRepository.save(activities1);
        long dif = compareTwoTimeStamps(activities1.getStart_time(), activities1.getEnd_time());
        if ((activities1.getActivity().contentEquals("Sleeping")) && dif > 43200) {
            return activities1;
        } else {
            if ((activities1.getActivity().contentEquals("Leaving")) && dif > 43200) {
                return activities1;
            } else {
                if ((activities1.getActivity().contentEquals("Toileting")) && dif > 3600) {
                    return activities1;
                } else {
                    return null;
                }
            }
        }
    }

    public List<ActivitiesDTO> findallGood(Integer idcaregiver) {
        List<Activities> activities = activitiesRepository.findAll();
        List<ActivitiesDTO> myList = new ArrayList<>();
        for(Activities a : activities) {
            if (a.getPatient().getCaregiver().getIdcaregiver().equals(idcaregiver)) {
                ActivitiesDTO activitiesDTO = new ActivitiesDTO();
                activitiesDTO.setIdactivities(a.getIdactivities());
                activitiesDTO.setStart_time(a.getStart_time());
                activitiesDTO.setEnd_time(a.getEnd_time());
                activitiesDTO.setPatient(a.getPatient().getIdpatient());
                activitiesDTO.setActivity(a.getActivity());
                if (myFunc(activitiesDTO) != null) {
                    myList.add(activitiesDTO);
                }
            }
        }
        return myList;
    }

    public static long compareTwoTimeStamps(Timestamp start_time, Timestamp end_time) {
        long milliseconds1 = start_time.getTime();
        long milliseconds2 = end_time.getTime();

        long diff = milliseconds2 - milliseconds1;
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000);
        long diffHours = diff / (60 * 60 * 1000);
        long diffDays = diff / (24 * 60 * 60 * 1000);

        //long diffin = diffSeconds + diffMinutes * 60 + diffHours * 3600 + diffDays *3600 *24;

        return diffSeconds;
    }
}
