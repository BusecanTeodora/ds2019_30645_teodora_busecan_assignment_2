package com.example.springdemo.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "intake")
public class Intake {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idintake", unique = true, nullable = false)
    private Integer idintake;

    @ManyToOne
    @JoinColumn(name = "idpatient")
    private Patient patient;


    @Column(name = "period")
    private String period;

    public Intake(Integer idintake, String period) {
        this.idintake = idintake;
        this.period = period;
    }

    public Intake() {
    }

    public Integer getIdintake() {
        return idintake;
    }

    public void setIdintake(Integer idintake) {
        this.idintake = idintake;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
