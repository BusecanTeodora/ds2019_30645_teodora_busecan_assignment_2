package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.dto.UserTypeDTO;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Doctor;

public class CaregiverBuilder {

    public CaregiverBuilder() {
    }

    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver){
        DoctorDTO doctorDTO = new DoctorDTO();
        UserDTO userDTO = new UserDTO();
        UserDTO userDTO1 = new UserDTO();
        UserTypeDTO userTypeDTO = new UserTypeDTO();
        UserTypeDTO userTypeDTO1 = new UserTypeDTO();

        userTypeDTO.setUsertype(caregiver.getDoctor().getUser().getUserType().getUserType());
        userTypeDTO1.setUsertype(caregiver.getUser().getUserType().getUserType());

        userDTO1.setUserTypeDTO(userTypeDTO1);
        userDTO1.setUsername(caregiver.getUser().getUsername());
        userDTO1.setIduser(caregiver.getUser().getIduser());
        userDTO1.setPassword(caregiver.getUser().getPassword());

        userDTO.setIduser(caregiver.getDoctor().getUser().getIduser());
        userDTO.setPassword(caregiver.getDoctor().getUser().getPassword());
        userDTO.setUsername(caregiver.getDoctor().getUser().getUsername());
        userDTO.setUserTypeDTO(userTypeDTO);

        doctorDTO.setIddoctor(caregiver.getDoctor().getIddoctor());
        doctorDTO.setName(caregiver.getDoctor().getName());
        doctorDTO.setUserDTO(userDTO);


        return new CaregiverDTO(caregiver.getIdcaregiver(),caregiver.getName(),caregiver.getDate(),caregiver.getAddress(),caregiver.getGender(),doctorDTO,userDTO1);
    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO){
        return new Caregiver(
                caregiverDTO.getIdcaregiver(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthday(),caregiverDTO.getAddress(),caregiverDTO.getGender());
    }
}
