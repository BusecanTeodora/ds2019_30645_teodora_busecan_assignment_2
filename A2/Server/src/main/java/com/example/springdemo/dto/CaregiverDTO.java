package com.example.springdemo.dto;

import java.sql.Date;
import java.util.Objects;

public class CaregiverDTO {

    private Integer idcaregiver;
    private String name;
    private String birthday;
    private String address;
    private String gender;

    private DoctorDTO doctorDTO;
    private UserDTO userDTO;

    public CaregiverDTO() {
    }

    public CaregiverDTO(Integer idcaregiver, String name, String birthday, String address, String gender, DoctorDTO doctorDTO, UserDTO userDTO) {
        this.idcaregiver = idcaregiver;
        this.name = name;
        this.birthday = birthday;
        this.address = address;
        this.gender = gender;
        this.doctorDTO = doctorDTO;
        this.userDTO = userDTO;
    }

    public Integer getIdcaregiver() {
        return idcaregiver;
    }

    public void setIdcaregiver(Integer idcaregiver) {
        this.idcaregiver = idcaregiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public DoctorDTO getDoctorDTO() {
        return doctorDTO;
    }

    public void setDoctorDTO(DoctorDTO doctorDTO) {
        this.doctorDTO = doctorDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO that = (CaregiverDTO) o;
        return Objects.equals(idcaregiver, that.idcaregiver) &&
                Objects.equals(name, that.name) &&
                Objects.equals(birthday, that.birthday) &&
                Objects.equals(address, that.address) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(doctorDTO, that.doctorDTO) &&
                Objects.equals(userDTO, that.userDTO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idcaregiver, name, birthday, address, gender, doctorDTO, userDTO);
    }
}