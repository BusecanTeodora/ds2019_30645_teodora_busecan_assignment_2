package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.DrugDTO;
import com.example.springdemo.entities.Drug;

public class DrugBuilder {

        public DrugBuilder() {
        }

        public static DrugDTO generateDTOFromEntity(Drug drug){
            return new DrugDTO(drug.getIddrug(),drug.getName(),drug.getSideeffect(),drug.getDosage());
        }

        public static Drug generateEntityFromDTO(DrugDTO drugDTO){
            return new Drug(
                    drugDTO.getIddrug(),
                    drugDTO.getName(),
                    drugDTO.getSideeffect(),
                    drugDTO.getDosage());
        }
}
