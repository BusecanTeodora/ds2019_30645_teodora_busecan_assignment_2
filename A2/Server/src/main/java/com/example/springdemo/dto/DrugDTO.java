package com.example.springdemo.dto;

import java.util.Objects;

public class DrugDTO {

    private Integer iddrug;
    private String name;
    private String sideeffect;
    private Integer dosage;

    public DrugDTO() {
    }

    public DrugDTO(Integer iddrug, String name, String sideeffect, Integer dosage) {
        this.iddrug = iddrug;
        this.name = name;
        this.sideeffect = sideeffect;
        this.dosage = dosage;
    }

    public Integer getIddrug() {
        return iddrug;
    }

    public void setIddrug(Integer iddrug) {
        this.iddrug = iddrug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideeffect() {
        return sideeffect;
    }

    public void setSideeffect(String sideeffect) {
        this.sideeffect = sideeffect;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrugDTO drugDTO = (DrugDTO) o;
        return Objects.equals(iddrug, drugDTO.iddrug) &&
                Objects.equals(name, drugDTO.name) &&
                Objects.equals(sideeffect, drugDTO.sideeffect) &&
                Objects.equals(dosage, drugDTO.dosage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iddrug, name, sideeffect, dosage);
    }
}
