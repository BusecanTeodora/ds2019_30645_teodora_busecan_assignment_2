package com.example.springdemo.services;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.dto.builders.DoctorBuilder;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.repositories.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;
    private UserService userService;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository, UserService userService) {
        this.doctorRepository = doctorRepository;
        this.userService = userService;
    }



    public DoctorDTO findDoctorByUsername(String username){
      Integer doctorId = userService.findUserByUsername(username);
      Optional<Doctor> doctor = doctorRepository.findById(doctorId);
        return DoctorBuilder.generateDTOFromEntity(doctor.get());
    }

}
