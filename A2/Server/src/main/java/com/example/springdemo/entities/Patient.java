package com.example.springdemo.entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idpatient", unique = true, nullable = false)
    private Integer idpatient;

    @ManyToOne
    @JoinColumn(name = "idcaregiver")
    private Caregiver caregiver;

    @ManyToOne
    @JoinColumn(name = "iduser")
    private User user;

    @Column(name = "name")
    private String name;

    @Column(name = "birthday")
    private String date;

    @Column(name = "address")
    private String address;

    @Column(name = "gender")
    private String gender;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL)
    private Set<Intake> intakeSet;

    @OneToMany(mappedBy = "patient", cascade = CascadeType.ALL)
    private Set<Activities> activitiesSet;

    public Patient() {
    }

    public Patient(Integer idpatient, Caregiver caregiver, User user, String name, String date, String address, String gender) {
        this.idpatient = idpatient;
        this.caregiver = caregiver;
        this.user = user;
        this.name = name;
        this.date = date;
        this.address = address;
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Patient(Integer idpatient, String name, String date, String address, String gender) {
        this.idpatient = idpatient;
        this.name = name;
        this.date = date;
        this.address = address;
        this.gender = gender;
    }

    public Patient(Integer idpatient, String name) {
        this.idpatient = idpatient;
        this.name = name;
    }

    public Integer getIdpatient() {
        return idpatient;
    }

    public void setIdpatient(Integer idpatient) {
        this.idpatient = idpatient;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
