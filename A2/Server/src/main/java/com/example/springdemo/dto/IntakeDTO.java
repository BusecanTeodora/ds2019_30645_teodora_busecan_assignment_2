package com.example.springdemo.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

public class IntakeDTO {

    private Integer idintake;
    private String period;
    private String intake_intervals;
    private PatientDTO patientDTO;
    private List<DrugDTO> drugDTOs;

    public IntakeDTO() {
    }

    public IntakeDTO(Integer idintake, String period, String intake_intervals, PatientDTO patientDTO, List<DrugDTO> drugDTOs) {
        this.idintake = idintake;
        this.period = period;
        this.intake_intervals = intake_intervals;
        this.patientDTO = patientDTO;
        this.drugDTOs = drugDTOs;
    }

    public Integer getIdintake() {
        return idintake;
    }

    public void setIdintake(Integer idintake) {
        this.idintake = idintake;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getIntake_intervals() {
        return intake_intervals;
    }

    public void setIntake_intervals(String intake_intervals) {
        this.intake_intervals = intake_intervals;
    }

    public PatientDTO getPatientDTO() {
        return patientDTO;
    }

    public void setPatientDTO(PatientDTO patientDTO) {
        this.patientDTO = patientDTO;
    }

    public List<DrugDTO> getDrugDTOs() {
        return drugDTOs;
    }

    public void setDrugDTOs(List<DrugDTO> drugDTOs) {
        this.drugDTOs = drugDTOs;
    }
}