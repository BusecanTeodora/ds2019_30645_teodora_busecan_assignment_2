package com.example.springdemo.services;

import com.example.springdemo.dto.*;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.dto.builders.UserBuilder;
import com.example.springdemo.entities.*;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.repositories.IntakeRepository;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PatientService {

    private final PatientRepository patientRepository;
    private final UserRepository userRepository;
    private final CaregiverRepository caregiverRepository;
    private final IntakeRepository intakeRepository;
    private UserService userService;

    @Autowired
    public PatientService(IntakeRepository intakeRepository,PatientRepository patientRepository,UserService userService, UserRepository userRepository,CaregiverRepository caregiverRepository) {
        this.patientRepository = patientRepository;
        this.intakeRepository = intakeRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.caregiverRepository = caregiverRepository;
    }



    public PatientDTO findpatientById(Integer id){
        Optional<Patient> patient  = patientRepository.findById(id);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", id);
        }
        return PatientBuilder.generateDTOFromEntity(patient.get());
    }

    public List<PatientDTOinsert> findAll(){
        List<Patient> patients = patientRepository.findAll();
        List <PatientDTOinsert> patientDTOinsert = new ArrayList<>();
        for(Patient p: patients){
           PatientDTOinsert patientDTOinsert1 = new PatientDTOinsert(p.getIdpatient(),p.getName(),p.getDate(),p.getAddress(),p.getGender(),p.getCaregiver().getIdcaregiver(),p.getUser().getIduser());
           patientDTOinsert.add(patientDTOinsert1);
        }

        return patientDTOinsert;
    }

    public Integer insert(PatientDTOinsert patientDTOinsert) {
        Patient patient = new Patient();

        List<Patient> patients = patientRepository.findAll();
        for(Patient p: patients){
            if(p.getUser().getIduser() == patientDTOinsert.getIdUser()){
                return null;
            }
        }
        UserType userType = new UserType();
        userType.setUserType(3);
        User user = new User();
        user.setUsername(patientDTOinsert.getUsername());
        user.setPassword(patientDTOinsert.getPassword());
        user.setUserType(userType);

        Integer id = (userRepository.save(user)).getIduser();

        Caregiver caregiver = caregiverRepository.getOne(patientDTOinsert.getIdCaregiver());
        patient.setIdpatient(patientDTOinsert.getIdpatient());
        patient.setAddress(patientDTOinsert.getAddress());
        patient.setCaregiver(caregiver);
        patient.setGender(patientDTOinsert.getGender());
        patient.setDate(patientDTOinsert.getBirthday());
        patient.setUser(user);
        patient.setName(patientDTOinsert.getName());
        return patientRepository.save(patient).getIdpatient();
    }

    public Integer update(PatientDTOinsert patientDTOinsert) {
        Patient patient = new Patient();
        User user = userRepository.getOne(patientDTOinsert.getIdUser());
        Caregiver caregiver = caregiverRepository.getOne(patientDTOinsert.getIdCaregiver());
        patient.setIdpatient(patientDTOinsert.getIdpatient());
        patient.setAddress(patientDTOinsert.getAddress());
        patient.setCaregiver(caregiver);
        patient.setGender(patientDTOinsert.getGender());
        patient.setDate(patientDTOinsert.getBirthday());
        patient.setUser(user);
        patient.setName(patientDTOinsert.getName());
        return patientRepository.save(patient).getIdpatient();
    }

    public void delete(PatientDTOinsert patientDTOinsert){
        List<Intake> intakes = intakeRepository.findAll();
        for(Intake intake:intakes){
            if(intake.getPatient().getIdpatient()==patientDTOinsert.getIdpatient()){
                intakeRepository.delete(intake);
            }
        }
        this.patientRepository.deleteById(patientDTOinsert.getIdpatient());
    }


    public PatientDTOinsert findPatientByUsername(String username){
        Integer userId = userService.findUserByUsername(username);
        PatientDTOinsert patientDTOinsert1 = new PatientDTOinsert();
        List<Patient> patients = patientRepository.findAll();
        for(Patient p:patients){
            if(p.getUser().getIduser() == userId){
                 patientDTOinsert1 = new PatientDTOinsert(p.getIdpatient(),p.getName(),p.getDate(),p.getAddress(),p.getGender(),p.getCaregiver().getIdcaregiver(),p.getUser().getIduser());

            }
        }

        return patientDTOinsert1;
    }
}
