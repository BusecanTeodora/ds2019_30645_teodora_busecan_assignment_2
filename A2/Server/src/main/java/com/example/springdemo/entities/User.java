package com.example.springdemo.entities;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "iduser", unique = true, nullable = false)
    private Integer iduser;

    @ManyToOne
    @JoinColumn(name = "usertype")
    private UserType usertype ;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Doctor> doctorSet;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Patient> patientSet;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Caregiver> caregiverSet;

    public User(Integer iduser,  String username, String password, UserType usertype) {
        this.iduser = iduser;
        this.usertype = usertype;
        this.username = username;
        this.password = password;
    }

    public User() {
    }

    public User(Integer iduser,  String username, String password) {
        this.iduser = iduser;
        this.username = username;
        this.password = password;
    }

    public Integer getIduser() {
        return iduser;
    }

    public void setIduser(Integer iduser) {
        this.iduser = iduser;
    }

    public UserType getUserType() {
        return usertype;
    }

    public void setUserType(UserType userType) {
        this.usertype = userType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
