package com.example.springdemo.dto;

import java.sql.Date;

public class PatientDTOinsert {

    private Integer idpatient;
    private Integer idCaregiver;
    private Integer idUser;
    private String name;
    private String birthday;
    private String address;
    private String gender;
    private String username;
    private String password;
   // private Integer usertype;

    public PatientDTOinsert(Integer idpatient, Integer idCaregiver, Integer idUser, String name, String birthday, String address, String gender, String username, String password) {
        this.idpatient = idpatient;
        this.idCaregiver = idCaregiver;
        this.idUser = idUser;
        this.name = name;
        this.birthday = birthday;
        this.address = address;
        this.gender = gender;
        this.username = username;
        this.password = password;
    }

    public PatientDTOinsert() {
    }

    public PatientDTOinsert(Integer idpatient, String name, String birthday, String address, String gender, Integer idCaregiver, Integer idUser) {
        this.idpatient = idpatient;
        this.name = name;
        this.birthday = birthday;
        this.address = address;
        this.gender = gender;
        this.idCaregiver = idCaregiver;
        this.idUser = idUser;
    }

    public Integer getIdpatient() {
        return idpatient;
    }

    public void setIdpatient(Integer idpatient) {
        this.idpatient = idpatient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getIdCaregiver() {
        return idCaregiver;
    }

    public void setIdCaregiver(Integer idCaregiver) {
        this.idCaregiver = idCaregiver;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}