package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.dto.UserTypeDTO;
import com.example.springdemo.entities.Doctor;

public class DoctorBuilder {
    public DoctorBuilder() {
    }

    public static DoctorDTO generateDTOFromEntity(Doctor doctor){
        UserDTO userDTO = new UserDTO();
        UserTypeDTO userTypeDTO = new UserTypeDTO();

        userTypeDTO.setUsertype(doctor.getUser().getUserType().getUserType());
        userDTO.setIduser(doctor.getUser().getIduser());
        userDTO.setUsername(doctor.getUser().getUsername());
        userDTO.setPassword(doctor.getUser().getPassword());
        userDTO.setUserTypeDTO(userTypeDTO);
        return new DoctorDTO(doctor.getIddoctor(),doctor.getName(),userDTO);
    }

    public static Doctor generateEntityFromDTO(DoctorDTO doctorDTO){
        return new Doctor(
                doctorDTO.getIddoctor(),
                doctorDTO.getName());
    }
}
