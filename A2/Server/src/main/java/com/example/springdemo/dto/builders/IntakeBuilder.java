//package com.example.springdemo.dto.builders;
//
//import com.example.springdemo.dto.*;
//import com.example.springdemo.entities.Intake;
//
//public class IntakeBuilder {
//    public IntakeBuilder() {
//    }
//
//    public static IntakeDTO generateDTOFromEntity(Intake intake){
//        CaregiverDTO caregiverDTO = new CaregiverDTO();
//        UserTypeDTO userTypeDTOCareg = new UserTypeDTO();
//        UserDTO userDTOCaregiv = new UserDTO();
//
//        PatientDTO patientDTO = new PatientDTO();
//        UserDTO userDTO = new UserDTO();
//        UserTypeDTO userTypeDTO = new UserTypeDTO();
//
//        DoctorDTO doctorDTO = new DoctorDTO();
//        UserDTO userDTODoctor = new UserDTO();
//        UserTypeDTO userTypeDTODoctor = new UserTypeDTO();
//
//
//        DrugDTO drugDTO = new DrugDTO();
//
//        userTypeDTO.setUsertype(intake.getPatient().getUser().getUserType().getUserType());
//        userTypeDTOCareg.setUsertype(intake.getPatient().getCaregiver().getUser().getUserType().getUserType());
//        userTypeDTODoctor.setUsertype(intake.getPatient().getCaregiver().getDoctor().getUser().getUserType().getUserType());
//
//        userDTODoctor.setUserTypeDTO(userTypeDTODoctor);
//        userDTODoctor.setIduser(intake.getPatient().getCaregiver().getDoctor().getUser().getIduser());
//        userDTODoctor.setUsername(intake.getPatient().getCaregiver().getDoctor().getUser().getUsername());
//        userDTODoctor.setPassword(intake.getPatient().getCaregiver().getDoctor().getUser().getPassword());
//
//        doctorDTO.setUserDTO(userDTODoctor);
//        doctorDTO.setName(intake.getPatient().getCaregiver().getDoctor().getName());
//        doctorDTO.setIddoctor(intake.getPatient().getCaregiver().getDoctor().getIddoctor());
//
//        userDTOCaregiv.setUserTypeDTO(userTypeDTOCareg);
//        userDTOCaregiv.setIduser(intake.getPatient().getCaregiver().getUser().getIduser());
//        userDTOCaregiv.setUsername(intake.getPatient().getCaregiver().getUser().getUsername());
//        userDTOCaregiv.setPassword(intake.getPatient().getCaregiver().getUser().getPassword());
//
//        caregiverDTO.setAddress(intake.getPatient().getCaregiver().getAddress());
//        caregiverDTO.setBirthday(intake.getPatient().getCaregiver().getDate());
//        caregiverDTO.setGender(intake.getPatient().getCaregiver().getGender());
//        caregiverDTO.setName(intake.getPatient().getCaregiver().getName());
//        caregiverDTO.setIdcaregiver(intake.getPatient().getCaregiver().getIdcaregiver());
//        caregiverDTO.setDoctorDTO(doctorDTO);
//        caregiverDTO.setUserDTO(userDTOCaregiv);
//
//        userDTO.setPassword(intake.getPatient().getUser().getPassword());
//        userDTO.setUsername(intake.getPatient().getUser().getUsername());
//        userDTO.setIduser(intake.getPatient().getUser().getIduser());
//        userDTO.setUserTypeDTO(userTypeDTO);
//
//        patientDTO.setAddress(intake.getPatient().getAddress());
//        patientDTO.setBirthday(intake.getPatient().getDate());
//        patientDTO.setCaregiverDTO(caregiverDTO);
//        patientDTO.setGender(intake.getPatient().getGender());
//        patientDTO.setIdpatient(intake.getPatient().getIdpatient());
//        patientDTO.setName(intake.getPatient().getName());
//        patientDTO.setUserDTO(userDTO);
//
//        drugDTO.setIddrug(intake.getDrug().getIddrug());
//        drugDTO.setDosage(intake.getDrug().getDosage());
//        drugDTO.setName(intake.getDrug().getName());
//        drugDTO.setSideeffect(intake.getDrug().getSideeffect());
//
//
//        return new IntakeDTO(intake.getIdintake(),intake.getStarttime(),intake.getEndtime(),patientDTO,drugDTO);
//    }
//
//    public static Intake generateEntityFromDTO(IntakeDTO intakeDTO){
//        return new Intake(intakeDTO.getIdintake(),intakeDTO.getStarttime(),intakeDTO.getEndtime());
//    }
//}
