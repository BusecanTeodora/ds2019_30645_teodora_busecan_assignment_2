package com.example.springdemo.dto;

import java.util.Objects;

public class UserTypeDTO {

    private Integer usertype;

    public UserTypeDTO() {
    }

    public UserTypeDTO(Integer usertype) {
        this.usertype = usertype;
    }

    public Integer getUsertype() {
        return usertype;
    }

    public void setUsertype(Integer usertype) {
        this.usertype = usertype;
    }

}
