package com.example.springdemo.services;

import com.example.springdemo.dto.IntakeDTOView;
import com.example.springdemo.entities.Drug;
import com.example.springdemo.entities.Intake;
import com.example.springdemo.entities.IntakeDrugs;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.repositories.DrugRepository;
import com.example.springdemo.repositories.IntakeDrugsRepository;
import com.example.springdemo.repositories.IntakeRepository;
import com.example.springdemo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IntakeService {

    private final IntakeRepository intakeRepository;
    private final IntakeDrugsRepository intakeDrugsRepository;
    private final PatientRepository patientRepository;
    private final DrugRepository drugRepository;

    @Autowired
    public IntakeService(PatientRepository patientRepository,DrugRepository drugRepository,IntakeRepository intakeRepository,IntakeDrugsRepository intakeDrugsRepository) {
        this.intakeRepository = intakeRepository;
        this.patientRepository = patientRepository;
        this.drugRepository = drugRepository;
        this.intakeDrugsRepository = intakeDrugsRepository;
    }



    public List<IntakeDTOView> findAll(Integer idpatient) {

        List<Intake> intakes = intakeRepository.findAll();

//        List<String> drugs = new ArrayList<>();

        List<IntakeDrugs> intakeDrugs = intakeDrugsRepository.findAll();

//        List<String> intake_intervals = new ArrayList<>();
//        IntakeDTOView intakeDTOView = new IntakeDTOView();
     List<IntakeDTOView> intakeDTOViews = new ArrayList<>();
        Integer idintake = null;

        for(Intake i : intakes) {
            IntakeDTOView intakeDTOView = new IntakeDTOView();
            List<String> intake_intervals = new ArrayList<>();
            List<String> drugs = new ArrayList<>();
            if (i.getPatient().getIdpatient() == idpatient) {

                for (IntakeDrugs intakeDrugs1 : intakeDrugs) {
                    idintake = i.getIdintake();
                    if (intakeDrugs1.getIntake().getIdintake() == idintake) {
                        intakeDTOView.setPeriod(i.getPeriod());
                        intakeDTOView.setIdPatient(idpatient);
                        intakeDTOView.setIdintake(i.getIdintake());
                        intake_intervals.add(intakeDrugs1.getIntake_intervals());
                        drugs.add(intakeDrugs1.getDrug().getName());

                    }


                }

            }
            intakeDTOView.setIntake_intervals(intake_intervals);
            intakeDTOView.setDrugDTOs(drugs);
            if(intakeDTOView.getIdintake()!= null)
            intakeDTOViews.add(intakeDTOView);
        }
        return intakeDTOViews;
    }

    public void insert(IntakeDTOView intakeDTOView) {
        Intake intake = new Intake();

        List<IntakeDrugs> intakeDrugs1 = new ArrayList<>();
        Patient patient = new Patient();
        Drug drug = new Drug();
        Integer idPatient = intakeDTOView.getIdPatient();
        List<String> drugs = intakeDTOView.getDrugDTOs();
        List<String> interv = intakeDTOView.getIntake_intervals();
        patient = patientRepository.getOne(idPatient);

        intake.setPatient(patient);
        intake.setPeriod(intakeDTOView.getPeriod());
        intakeRepository.save(intake);

        int j = drugs.size();
        System.out.println(j);
        for(int i = 0; i<j;i++){
            IntakeDrugs intakeDrugs = new IntakeDrugs();
            Integer id = Integer.valueOf(drugs.get(i));
           intakeDrugs.setDrug(drugRepository.getOne(id));
           intakeDrugs.setIntake(intake);
           intakeDrugs.setIntake_intervals(interv.get(i));

           intakeDrugsRepository.save(intakeDrugs);
        }


    }
}
