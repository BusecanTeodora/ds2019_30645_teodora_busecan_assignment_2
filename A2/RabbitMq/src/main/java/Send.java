import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import jdk.nashorn.internal.parser.JSONParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;

public class Send {

    private final static String QUEUE_NAME = "hello";
    

    public static void main(String[] argv) throws Exception {
         ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        BufferedReader br;

        File file = new File("C:\\Users\\DeLL\\Desktop\\Ass2\\src\\main\\resources\\activities.txt");

        try {
            br =  new BufferedReader(new FileReader(file));
            String line = br.readLine();
            while (line != null) {
                try (Connection connection = factory.newConnection();
                     Channel channel = connection.createChannel()) {
                    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
                    String[] detailds = line.split("\t");

                    Integer idPatient = Integer.valueOf(detailds[0]);
                    Timestamp start_time = Timestamp.valueOf(detailds[1]);
                    Timestamp end_time = Timestamp.valueOf(detailds[2]);
                    String name = detailds[3];
                    Activities activities = new Activities(idPatient,start_time,end_time,name);
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    System.out.println(timestamp);
                    String message = objectMapper.writeValueAsString(activities);
                    channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
                    System.out.println(" [x] Sent '" + message + "'");
                    line = br.readLine();
                    Thread.sleep(1000);
                }
            }
            br.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }


    }
}