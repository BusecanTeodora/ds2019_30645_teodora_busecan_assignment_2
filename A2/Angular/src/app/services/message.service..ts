import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {REST_API} from '../common/API';
import 'rxjs-compat/add/operator/catch';
import {MatDialog} from '@angular/material';
import {UserLogIn} from '../model/UserLogIn';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  public logIn(user: UserLogIn): Observable<number> {
    return this.http.post<any>(REST_API + 'user/login', user);
  }
}

