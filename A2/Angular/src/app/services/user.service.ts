import { Injectable } from '@angular/core';
import {REST_API} from '../common/API';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Patient} from '../model/Patient';
import {Caregiver} from '../model/Caregiver';
import {Doctor} from '../model/Doctor';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private doctorURL: string;
  private caregiverURL: string;
  private patientURL: string;
  private loggedUser: string;
  private loggedCaregiver: string;

  constructor(private http: HttpClient) {
  }

  public getCaregiver(username: String) {
    return this.http.post<Caregiver>(REST_API + 'caregiver/getCaregiver', username);
  }

  public getPatient(username: string) {
    return this.http.post<Patient>(REST_API + 'patient/getPatient', username);
  }

  public getDoctor(username: string) {
    return this.http.post<Doctor>(REST_API + 'doctor/getDoctor', username);
  }

}
