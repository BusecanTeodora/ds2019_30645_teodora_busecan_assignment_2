import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Drug} from '../model/Drug';
import {REST_API} from '../common/API';
import {Observable, throwError} from 'rxjs';
import 'rxjs-compat/add/operator/catch';
import {MatDialog} from '@angular/material';
import {ErrorService} from '../utils/error-service';


@Injectable({
  providedIn: 'root'
})
export class DrugService {

  constructor(private http: HttpClient, private dialog: MatDialog) {
  }

  getDrugs(): Observable<Drug[]> {
    return this.http.get<Drug[]>(REST_API + 'drug/drugs')
      .catch((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  insertDrug(drug: Drug): Observable<Drug> {
    return this.http.post<any>(REST_API + 'drug', drug)
      .catch((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  }

  public deleteDrug(drug: Drug) {
    return this.http.post<any>(REST_API + 'drug/delete', drug);
  }

  // getUserById(id: number): Observable<UserView> {
  //   return this.http.get<UserView>(REST_API + 'person' + id)
  //     .catch((e: any) => throwError(ErrorService.handleError(e, this.dialog)));
  // }
  public modifyDrug(drug: Drug) {
    return this.http.post<any>(REST_API + 'drug/modify', drug);
  }
}
