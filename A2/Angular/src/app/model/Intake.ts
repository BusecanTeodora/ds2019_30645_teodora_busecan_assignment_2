export class Intake {
  idintake: number;
  period: string;
  intake_intervals: string[];
  idPatient: number;
  drugDTOs: string[];

}
