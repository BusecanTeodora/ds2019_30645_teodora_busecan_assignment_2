import { Component, OnInit } from '@angular/core';
import {PatientService} from '../services/patient.service';
import {Router} from '@angular/router';
import {Patient} from '../model/Patient';

@Component({
  selector: 'app-patient-modify',
  templateUrl: './patient-modify.component.html',
  styleUrls: ['./patient-modify.component.css']
})
export class PatientModifyComponent implements OnInit {
  patient: Patient;
  patients = [];

  constructor(private router: Router, private patientService: PatientService) { }

  ngOnInit() {
    this.patient = JSON.parse(localStorage.getItem('patient_modify'));
    console.log(this.patient);
  }
  confirmModify() {
    this.patientService.modifyPatient(this.patient).subscribe(data => this.router.navigate(['patients']));

  }


}
